package bf.gov.formation.client.dto;

import bf.gov.formation.client.utilities.ECstatut;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * Dto de client.
 */
@Getter
@Setter
@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false, onlyExplicitlyIncluded = true)
public class ClientDto implements Serializable {
    /**
     * numero du client.
     */
    @EqualsAndHashCode.Include
    private String numero;
    /**
     * nom du client.
     */
    private String nomClient;
    /**
     * prenom du client.
     */
    private String prenomClient;
    /**
     * date de naissance.
     */
    private String dateNaissance;
    /**
     * Etat du client.
     */
    private ECstatut statut;
}
