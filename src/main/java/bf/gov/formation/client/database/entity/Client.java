package bf.gov.formation.client.database.entity;

import bf.gov.formation.client.utilities.ECstatut;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * Entite de creation d'un client.
 * @author yigalpen
 * @since oct 2023
 */

@Entity
@Table(name = "client")
@Getter
@Setter
@Builder
@EqualsAndHashCode(onlyExplicitlyIncluded = true, callSuper = false)
public class Client implements Serializable {
    /**
     * numero du client.
     */
    @Id
    @EqualsAndHashCode.Include
    @Column(name = "numero")
    private String numero;
    /**
     * nom du client.
     */
    @Column(name = "nom")
    private String nomClient;
    /**
     * prenom du client.
     */
    @Column(name = "prenom")
    private String prenomClient;
    /**
     * date de naissance.
     */
    @Column(name = "date_naissance")
    private LocalDate dateNaissance;
    /**
     * Etat du client.
     */
    @Enumerated(EnumType.STRING)
    @Column(name = "statut")
    private ECstatut statut;
}
