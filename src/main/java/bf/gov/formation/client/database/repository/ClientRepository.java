package bf.gov.formation.client.database.repository;

import bf.gov.formation.client.database.entity.Client;
import bf.gov.formation.client.utilities.ECstatut;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ClientRepository extends JpaRepository<Client, String> {
    /**
     * retourne les clients en fonction du statut.
     * @param statut
     * @return liste des clients.
     */
    List<Client> findByStatut(ECstatut statut);
}
