package bf.gov.formation.client.controller;

import bf.gov.formation.client.dto.ClientDto;
import bf.gov.formation.client.service.ClientService;
import bf.gov.formation.client.utilities.ECstatut;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/formation")
@RequiredArgsConstructor
public class ClientController {

    private final ClientService clientService;

    @GetMapping("/clients")
    public ResponseEntity<List<ClientDto>> fetchClientList() {
        return new  ResponseEntity<>(clientService.fetchClients(ECstatut.ACTIF), HttpStatus.OK);
    }

}
