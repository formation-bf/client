package bf.gov.formation.client.service;

import bf.gov.formation.client.database.repository.ClientRepository;
import bf.gov.formation.client.dto.ClientDto;
import bf.gov.formation.client.utilities.ECstatut;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.stereotype.Service;

import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Service de gestion du client/
 */

@Service
@RequiredArgsConstructor
@Slf4j
public class ClientService {

    private final ClientRepository clientRepository;

    final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");

    /**
     * Recupere la liste des clients.
     * @param statut : statut
     * @return liste de clients
     */
    public List<ClientDto> fetchClients(ECstatut statut) {
        return clientRepository.findByStatut(ECstatut.ACTIF)
                .stream().map(client -> ClientDto.builder()
                        .numero(client.getNumero()).nomClient(client.getNomClient())
                        .prenomClient(client.getPrenomClient())
                        .dateNaissance(client.getDateNaissance().format(formatter)).build())
                .collect(Collectors.toList());
    }
}
