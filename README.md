**I. CREATION DU PROJET CLIENT**

###### _**Option de creation :_** 
projet : maven 
langage : java
version spring boot : 3.1.4
packaging : jar
java 17

###### _**starter de creation :_** 
Lombok : pour ka
spring web
JPA
Spring - security
H2 
postgres

**I. CONFIGURATION DU PROJET**
1 - Modification du fichier de configuration "application.properties"
    - modification du fichier properties en .yaml
    - Modification du port de 8080 par defaut a 8081
    - creation du context en ajoutant /formation-springboot
